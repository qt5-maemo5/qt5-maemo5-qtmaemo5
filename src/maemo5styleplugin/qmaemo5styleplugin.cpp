#include <QtWidgets>
#include <QMaemo5Style>

#include "qmaemo5styleplugin.h"

QStringList Maemo5StylePlugin::keys() const
{
    return QStringList() << "Maemo5";
}

QStyle *Maemo5StylePlugin::create(const QString &key)
{
    if (key.toLower() == "maemo5")
        return new QMaemo5Style();
    return 0;
} 
