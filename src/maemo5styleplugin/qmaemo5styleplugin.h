#include <QtWidgets>
#include <QMaemo5Style>

#ifndef MAEMO5STYLEPLUGIN_H
#define MAEMO5STYLEPLUGIN_H

#include <QStylePlugin>

class QStringList;
class QStyle;

class Maemo5StylePlugin : public QStylePlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QStyleFactoryInterface" FILE "maemo5style.json")

public:
    Maemo5StylePlugin() {}

    QStringList keys() const;
    QStyle *create(const QString &key);
};

#endif //MAEMO5STYLEPLUGIN_H