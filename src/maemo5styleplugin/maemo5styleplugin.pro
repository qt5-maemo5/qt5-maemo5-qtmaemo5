TARGET      = maemo5styleplugin

PLUGIN_TYPE = styles
PLUGIN_CLASS_NAME = QMaemo5StylePlugin
load(qt_plugin)

QT          = core-private gui widgets-private maemo5

QMAKE_CXXFLAGS += $$QT_CFLAGS_QGTKSTYLE
LIBS += $$QT_LIBS_QGTKSTYLE

HEADERS     = qmaemo5styleplugin.h
SOURCES     = qmaemo5styleplugin.cpp
OTHER_FILES += maemo5style.json


